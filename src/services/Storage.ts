import { Service } from "typedi";
import { Storage } from "@google-cloud/storage";
import { FileUpload } from "graphql-upload";

@Service()
export default class StorageService {
  private storage: Storage;
  private bucket_name: string;

  constructor() {
    this.storage = new Storage({
      keyFilename: __dirname + "../../../config/service_account.json",
    });
    this.bucket_name = "projectube-vc.appspot.com";
  }

  public getStorage(): Storage {
    return this.storage;
  }

  public uploadPrivateFile(file: FileUpload): Promise<boolean> {
    return new Promise((res) => {
      file
        .createReadStream()
        .pipe(
          this.storage
            .bucket(this.bucket_name)
            .file(file.filename)
            .createWriteStream()
        )
        .on("finish", () => {
          res(true);
        })
        .on("error", () => {
          res(false);
        });
    });
  }

  public async uploadPublicFile(file: FileUpload): Promise<string> {
    const fileRef = this.storage.bucket(this.bucket_name).file(file.filename);
    const result = await new Promise((res) => {
      file
        .createReadStream()
        .pipe(fileRef.createWriteStream())
        .on("finish", async () => {
          await fileRef.makePublic();
          res(true);
        })
        .on("error", (err) => {
          throw err;
        });
    });

    if (!result) {
      throw new Error("Somethign went wrong while upload file!");
    }

    const url = fileRef.publicUrl();
    return url;
  }

  public async getFileUrl(name: string): Promise<string> {
    const url = (
      await this.storage
        .bucket(this.bucket_name)
        .file(name)
        .getSignedUrl({
          action: "read",
          expires: Number(new Date()) + 1000 * 60 * 60, // 1 hour
        })
    )[0];

    return url;
  }
}
