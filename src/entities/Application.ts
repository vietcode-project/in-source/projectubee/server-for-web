import { Field, ID, ObjectType } from "type-graphql";
import { Column, Entity, ManyToOne } from "typeorm";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Recruitment } from "./Recruitment";
import { User } from "./User";

@Entity()
@ObjectType()
export class Application extends AbstractBaseEntity {
  @Field(() => ID)
  id: string;

  @ManyToOne(() => Recruitment, { onDelete: "CASCADE" })
  @Field(() => Recruitment)
  recruitment: Promise<Recruitment>;

  @ManyToOne(() => User, { onDelete: "CASCADE" })
  @Field(() => User)
  user: Promise<User>;

  @Column("text", { array: true, nullable: true })
  @Field(() => [String], { nullable: true })
  screen_answers?: string[];
}
