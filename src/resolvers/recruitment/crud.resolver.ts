import { AuthenticationError, UserInputError } from "apollo-server";
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Service } from "typedi";
import { FindOneOptions, Raw } from "typeorm";
import { Recruitment } from "../../entities/Recruitment";
import OrgService from "../../services/Org";
import RecruitService from "../../services/Recruitment";
import { CreateRecruitInput, UpdateRecruitInput } from "../inputs/Recruitment";
import { Org } from "../../entities/Org";
import { ContextType } from "../../types/Context";

@Service()
@Resolver()
export class RecruitCRUDResolver {
  constructor(private recruitService: RecruitService) {}

  @Authorized("org")
  @Mutation(() => Recruitment)
  async create_recruitment(
    @Ctx() ctx: ContextType,
    @Arg("data") data: CreateRecruitInput
  ) {
    return await this.recruitService.create(ctx.user as Org, data);
  }

  @Query(() => Recruitment)
  async recruitment(@Arg("recruitment_id") recruitment_id: string) {
    return this.recruitService.findById(recruitment_id);
  }

  @Query(() => [Recruitment])
  async recruitments(
    @Arg("limit", { nullable: true }) take: number = 10,
    @Arg("offset", { nullable: true }) skip: number = 0
  ) {
    return this.recruitService.findAll({
      take,
      skip,
    });
  }

  @Authorized("org")
  @Mutation(() => Recruitment)
  async update_recruitment(
    @Ctx() ctx: ContextType,
    @Arg("recruitment_id") recruitment_id: string,
    @Arg("data") data: UpdateRecruitInput
  ) {
    const rec = await this.recruitService.findById(recruitment_id);
    if (!rec)
      throw new UserInputError(
        "Không tìm thấy bài tuyển dụng với id: " + recruitment_id
      );
    if ((await rec.org).id !== ctx.user.id)
      throw new AuthenticationError("Forbidden");

    await this.recruitService.update(recruitment_id, data);
    return await this.recruitService.findById(recruitment_id);
  }

  @Authorized("org")
  @Mutation(() => Boolean)
  async delete_recruitment(
    @Ctx() ctx: ContextType,
    @Arg("recruitment_id") recruitment_id: string
  ) {
    const rec = await this.recruitService.findById(recruitment_id);
    if (!rec)
      throw new UserInputError(
        "Không tìm thấy bài tuyển dụng với id: " + recruitment_id
      );
    if ((await rec.org).id !== ctx.user.id)
      throw new AuthenticationError("Forbidden");

    try {
      await this.recruitService.delete(recruitment_id);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
