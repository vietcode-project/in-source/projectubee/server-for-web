import { AuthenticationError } from "apollo-server";
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";

import { Org } from "../../entities/Org";
import OrgService from "../../services/Org";
import { ContextType } from "../../types/Context";
import { UpdateOrgInput } from "../inputs/Org";

@Resolver()
export class MyOrgResolver {
  constructor(private orgService: OrgService) {}

  @Authorized()
  @Mutation(() => Org, { nullable: true })
  async update_org(@Ctx() ctx: ContextType, @Arg("data") data: UpdateOrgInput) {
    if (data.avatar) {
      await this.orgService.updatePhoto(
        ctx.user as Org,
        data.avatar,
        "avatarUrl"
      );
      delete data.avatar;
    }

    if (data.cover) {
      await this.orgService.updatePhoto(
        ctx.user as Org,
        data.cover,
        "coverUrl"
      );
      delete data.cover;
    }

    await this.orgService.update(ctx.user.id, data);

    return this.orgService.findById(ctx.user.id);
  }
}
