import { Service } from "typedi";
import {
  DeleteResult,
  FindManyOptions,
  RemoveOptions,
  UpdateResult,
  SaveOptions,
  getConnection,
} from "typeorm";
import { Org } from "../entities/Org";
import { Recruitment } from "../entities/Recruitment";
import { FileUpload } from "graphql-upload";
import StorageService from "./Storage";
import { v4 } from "uuid";
import {
  CreateRecruitInput,
  UpdateRecruitInput,
} from "../resolvers/inputs/Recruitment";

@Service()
export default class RecruitService {
  constructor(private storageService: StorageService) {}

  public async findAll(
    options?: FindManyOptions<Recruitment>
  ): Promise<Recruitment[]> {
    return await Recruitment.find(options);
  }

  public async findById(
    id: string,
    options?: FindManyOptions<Recruitment>
  ): Promise<Recruitment> {
    return await Recruitment.findOneOrFail(id, options);
  }

  public async create(
    org: Org,
    data: CreateRecruitInput
  ): Promise<Recruitment> {
    const newRecruit = Recruitment.create({ ...data });
    await Recruitment.save(newRecruit);

    if (data.cover_url) {
      await this.updatePhoto(newRecruit, data.cover_url, "cover_url");
    }

    await Promise.all([
      getConnection()
        .createQueryBuilder()
        .relation(Recruitment, "org")
        .of(newRecruit)
        .set(org),
    ]);
    return newRecruit;
  }

  public async delete(
    id: string,
    options?: RemoveOptions
  ): Promise<DeleteResult> {
    return await Recruitment.delete(id, options);
  }

  public async update(
    id: string,
    data: UpdateRecruitInput,
    options?: SaveOptions
  ): Promise<UpdateResult> {
    if (data.cover_url) {
      const recruitment = await this.findById(id);
      await this.updatePhoto(recruitment, data.cover_url, "cover_url");
    }
    return await Recruitment.update(id, data, options);
  }

  async updatePhoto(
    self: Recruitment,
    file: FileUpload,
    prop: "cover_url"
  ): Promise<string> {
    file = await file;

    const nameParts = file.filename.split(".");
    const extension = nameParts[nameParts.length - 1];
    const filename = v4() + "." + extension;

    file.filename = "/recruitment/" + self.id + "/" + filename;

    const photoUrl = await this.storageService.uploadPublicFile(file);
    self[prop] = photoUrl;
    await self.save();

    return photoUrl + "?dummy=1234";
  }
}
