import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Service } from "typedi";

import { User } from "../../entities/User";
import { ContextType } from "../../types/Context";
import { UpdateProfileInput } from "../inputs/User";
import UserService from "../../services/User";
import StorageService from "../../services/Storage";
import NotificationService from "../../services/Notification";

import Account from "../../entities/Account";

@Resolver()
@Service()
export class MeResolver {
  constructor(
    private userService: UserService,
    private notificationService: NotificationService,
    private storageService: StorageService
  ) {}

  @Authorized()
  @Query(() => Account)
  async me(@Ctx() ctx: ContextType): Promise<Account> {
    return ctx.user;
  }

  @Authorized()
  @Mutation(() => User)
  async update_profile(
    @Arg("data") data: UpdateProfileInput,
    @Ctx() ctx: ContextType
  ) {
    const payload: Partial<
      Pick<User, "name" | "avatarUrl" | "phone_number">
    > & { avatar?: any } = { ...data };

    if (data.avatar) {
      data.avatar = await data.avatar;
      data.avatar.filename = ctx.user.id + "/avatar";
      const url = await this.storageService.uploadPublicFile(data.avatar);
      payload.avatarUrl = url;
      delete payload.avatar;
    }

    try {
      await this.userService.updateProfile(ctx.user as User, payload);
    } catch (err) {
      console.error(err);
      return new Error("Something went wrong :(");
    }

    return this.userService.findByEmail(ctx.user.email);
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async subscribe_org(@Ctx() ctx: ContextType, @Arg("org_id") org_id: string) {
    await this.userService.subscribeOrg(ctx.user as User, org_id);
    return true;
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async subscribe_event(
    @Ctx() ctx: ContextType,
    @Arg("event_id") event_id: string
  ) {
    await this.userService.subscribeEvent(ctx.user as User, event_id);
    return true;
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async unsubscribe_org(
    @Ctx() ctx: ContextType,
    @Arg("org_id") org_id: string
  ) {
    await this.userService.unsubscribeOrg(ctx.user as User, org_id);
    return true;
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async unsubscribe_event(
    @Ctx() ctx: ContextType,
    @Arg("event_id") event_id: string
  ) {
    await this.userService.unsubscribeEvent(ctx.user as User, event_id);
    return true;
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async follow_user(
    @Ctx() ctx: ContextType,
    @Arg("user_id") user_id: string
  ): Promise<boolean> {
    await this.userService.followUser(ctx.user as User, user_id);

    const NotiUser: User = await this.userService.findById(user_id);
    await this.notificationService.sendFollowNoti(NotiUser, {
      follower: ctx.user as User,
    });

    return true;
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async unfollow_user(
    @Ctx() ctx: ContextType,
    @Arg("user_id") user_id: string
  ): Promise<boolean> {
    await this.userService.unfollowUser(ctx.user as User, user_id);
    const targetNoti = await this.notificationService.findByFollower(user_id);
    if (!targetNoti) {
      return true;
    }
    await this.notificationService.delete(targetNoti.id);
    return true;
  }
}
