import { GraphQLUpload } from "apollo-server";
import { Length, MaxLength } from "class-validator";
import { FileUpload } from "graphql-upload";
import { Field, InputType } from "type-graphql";
import { StringFilter } from "./Filter";

@InputType()
export class CreateEventInput {
  @Field()
  @MaxLength(40)
  name: string;

  @Field()
  description: string;

  @Field({ nullable: true })
  register_link: string;

  @Field({ nullable: true })
  start_time: Date;

  @Field({ nullable: true })
  end_time: Date;

  @Field({ nullable: true })
  location: string;

  @Field(() => GraphQLUpload, { nullable: true })
  cover: FileUpload;

  @Field(() => [String], { nullable: true })
  categories: string[];
}

@InputType()
export class UpdateEventInput {
  @Field({ nullable: true })
  name: string;

  @Field(() => GraphQLUpload, { nullable: true })
  cover: FileUpload;

  @Field({ nullable: true })
  start_time: Date;

  @Field({ nullable: true })
  end_time: Date;

  @Field({ nullable: true })
  location: string;

  @Field({ nullable: true })
  description: string;

  @Field({ nullable: true })
  register_link: string;

  @Field(() => [String], { nullable: true })
  categories: string[];
}

@InputType()
export class EventFilterInput {
  @Field({ nullable: true })
  name: StringFilter;

  @Field(() => [String], { nullable: true })
  categories: string[];

  @Field({ nullable: true })
  location: StringFilter;

  @Field({ nullable: true })
  start_time: string;
}
