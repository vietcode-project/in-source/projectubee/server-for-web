import { UserInputError } from "apollo-server-express";
import {
  Arg,
  Authorized,
  Ctx,
  Field,
  InputType,
  Mutation,
  ObjectType,
  Resolver,
} from "type-graphql";
import Account from "../entities/Account";
import { Org } from "../entities/Org";
import { User } from "../entities/User";
import AccountService from "../services/Account";
import { AuthService } from "../services/Auth";
import MailService from "../services/Mail";
import { ContextType } from "../types/Context";
import { UserRegisterInput } from "./inputs/Auth";

@InputType()
export class CredentialInput {
  @Field()
  email: string;

  @Field()
  password: string;
}

@ObjectType()
export class Tokens {
  @Field(() => String)
  access_token: string;

  @Field(() => String)
  refresh_token: string;
}

@Resolver()
export abstract class AuthResolver {
  // TODO Write description
  /**
   * Description
   */

  constructor(
    private authService: AuthService,
    private mailService: MailService,
    private accountService: AccountService
  ) {}

  @Mutation(() => Account)
  async register(
    @Arg("type") type: string,
    @Arg("data") data: UserRegisterInput,
    @Ctx() ctx: ContextType
  ) {
    const newUser = await this.authService.register({
      ...data,
      type: type,
      signup_host: ctx.req.protocol + "://" + ctx.req.hostname,
    });
    await this.mailService.sendConfirmationEmail(newUser as User | Org);

    return newUser;
  }

  @Mutation(() => Tokens)
  async signin(
    @Arg("credential", { nullable: true }) credential: CredentialInput,
    @Arg("refresh_token", { nullable: true }) refresh_token: string,
    @Arg("confirm_id", { nullable: true }) confirm_id: string
  ) {
    if (credential) {
      let token: Tokens = await this.authService.signinWithCredential(
        credential.email,
        credential.password
      );
      return token;
    } else if (refresh_token) {
      let token: Tokens = await this.authService.signinWithToken(refresh_token);
      return token;
    } else if (confirm_id) {
      let token: Tokens = await this.authService.signinWithConfirmID(
        confirm_id
      );
      return token;
    } else {
      throw new UserInputError(
        "Must provide credential or refresh token or confirm_id"
      );
    }
  }

  @Authorized()
  @Mutation(() => Boolean)
  async signout_all_devices(@Ctx() ctx: ContextType): Promise<boolean> {
    const authorization = ctx.req.headers.authorization;

    if (authorization) {
      return this.authService.signoutAllDevices(authorization);
    } else {
      throw new Error("Must provide access token");
    }
  }

  @Authorized()
  @Mutation(() => Boolean)
  async signout(
    @Arg("refresh_token") refresh_token: string,
    @Ctx() ctx: ContextType
  ): Promise<boolean> {
    if (!refresh_token) throw new UserInputError("Must provide refresh token");
    const authorization = ctx.req.headers.authorization;

    if (authorization) {
      return this.authService.signout(refresh_token, authorization);
    } else {
      throw new Error("Must provide access token");
    }
  }

  @Mutation(() => Boolean)
  async resend_verify_email(
    @Ctx() ctx: ContextType,
    @Arg("email") email: string
  ): Promise<boolean> {
    const account: Account = await this.accountService.findByEmail(email);
    if (!account) throw new Error("Account does not exist!");

    const confirm_id = await this.authService.getNewConfirmID(email);

    account.confirm_id = confirm_id;

    await this.mailService.sendConfirmationEmail(account as User | Org);

    return true;
  }
}
