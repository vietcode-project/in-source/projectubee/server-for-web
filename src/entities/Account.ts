import { BeforeInsert, Column, Entity, TableInheritance } from "typeorm";
import { IsEmail } from "class-validator";
import bcrypt from "bcrypt";
import { Field, InterfaceType } from "type-graphql";
import { AbstractBaseEntity } from "./AbstractBaseEntity";

@Entity()
@TableInheritance({ column: { name: "type", type: "varchar" } })
@InterfaceType()
export default abstract class Account extends AbstractBaseEntity {
  @Field()
  id: string;

  @Column()
  @Field()
  type: string;

  @Column({ unique: true })
  @IsEmail()
  @Field()
  email: string;

  @Column()
  password!: string;

  @Column({ default: false })
  confirmed: boolean;

  confirm_id?: string;

  @Column("text")
  signup_host: string;

  @BeforeInsert()
  private async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }
}
