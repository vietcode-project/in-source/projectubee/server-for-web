import { Response, Request } from "express";
import Account from "../entities/Account";

export type ContextType = {
  res: Response;
  req: Request;
  user?: Account;
};
