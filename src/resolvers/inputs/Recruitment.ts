import { Field, InputType } from "type-graphql";
import { FileUpload } from "graphql-upload";
import { GraphQLUpload } from "apollo-server";

@InputType()
export class CreateRecruitInput {
  @Field()
  title: string;

  @Field({ nullable: true })
  deadline: Date;

  @Field(() => GraphQLUpload, { nullable: true })
  cover_url: FileUpload;

  @Field(() => [String], { nullable: true })
  positions: string[];

  @Field(() => [String], { nullable: true })
  screen_questions: string[];

  @Field({ nullable: true })
  job_description: string;

  @Field({ nullable: true })
  location: string; 
}

@InputType()
export class UpdateRecruitInput {
  @Field({nullable: true})
  title: string;

  @Field({ nullable: true })
  deadline: Date;

  @Field(() => GraphQLUpload, { nullable: true })
  cover_url: FileUpload;

  @Field(() => [String], { nullable: true })
  positions: string[];

  @Field(() => [String], { nullable: true })
  screen_questions: string[];

  @Field({ nullable: true })
  job_description: string;

  @Field({ nullable: true })
  location: string; 

  @Field()
  isOpen: boolean;
}

@InputType()
export class RecruitFilterInput {
  @Field(() => [String], { nullable: true })
  positions: string[];
}
