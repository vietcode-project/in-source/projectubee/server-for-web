import { Service } from "typedi";
import {
  DeleteResult,
  FindOneOptions,
  getConnection,
  RemoveOptions,
} from "typeorm";
import { FollowNotification, Notification } from "../entities/Noti";
import { User } from "../entities/User";
import UserService from "./User";

@Service()
export default class NotificationService {
  constructor(private userService: UserService) {}

  public async findById(
    id: string,
    options?: FindOneOptions<Notification>
  ): Promise<Notification | null> {
    return await Notification.findOneOrFail(id, options);
  }

  public async findByFollower(
    user_id: string,
    options?: FindOneOptions<Notification>
  ): Promise<Notification | null> {
    const Noti = await Notification.findOneOrFail({
      ...options,
      where: {
        user: {
          id: user_id,
        },
      },
    });
    return Noti;
  }

  public async sendFollowNoti(
    u: User,
    data: { follower: User }
  ): Promise<Notification> {
    const newNoti = FollowNotification.create();
    await FollowNotification.save(newNoti);

    await Promise.all([
      getConnection()
        .createQueryBuilder()
        .relation(FollowNotification, "user")
        .of(newNoti)
        .set(u),
      getConnection()
        .createQueryBuilder()
        .relation(FollowNotification, "follower")
        .of(newNoti)
        .set(data.follower),
    ]);

    return this.findById(newNoti.id);
  }

  public async delete(
    id: string,
    options?: RemoveOptions
  ): Promise<DeleteResult> {
    return await Notification.delete(id, options);
  }
}
