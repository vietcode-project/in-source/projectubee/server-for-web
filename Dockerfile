FROM node:14-alpine

WORKDIR /app
ENV NODE_ENV=production

RUN npm i -g npm

COPY package.json .
RUN npm i
RUN npm i -g typescript

COPY . .
RUN npm run build

CMD ["npm", "start"]
# CMD ["npm", "start"]
