import { FileUpload } from "graphql-upload";
import { Service } from "typedi";
import {
  DeleteResult,
  FindManyOptions,
  FindOneOptions,
  getConnection,
  Raw,
  SaveOptions,
} from "typeorm";
import { v4 } from "uuid";
import { Org } from "../entities/Org";
import { User } from "../entities/User";
import { CreateOrgInput } from "../resolvers/inputs/Org";
import StorageService from "./Storage";

@Service()
export default class OrgService {
  constructor(private storageService: StorageService) {}

  public async create(user: User, data: CreateOrgInput): Promise<Org> {
    const newOrg = Org.create(data);
    try {
      await Org.save(newOrg);

      if (data.avatar) {
        await this.updatePhoto(newOrg, data.avatar, "avatarUrl");
      }

      if (data.cover) {
        await this.updatePhoto(newOrg, data.cover, "coverUrl");
      }

      return newOrg;
    } catch (err) {
      console.error(err[0]);
      throw new Error(err[0].constraints[Object.keys(err[0].constraints)[0]]);
    }
  }

  public async findAll(options?: FindManyOptions<Org>): Promise<Org[]> {
    return await Org.find({
      ...options,
      order: {
        name: "ASC",
      },
    });
  }

  public async findById(
    id: string,
    options?: FindOneOptions<Org>
  ): Promise<Org | null> {
    return await Org.findOneOrFail(id, options);
  }

  public async update(
    id: string,
    data: Partial<Pick<Org, "name" | "contact" | "description">>,
    options?: SaveOptions
  ) {
    return Org.update(id, data, options);
  }

  public async delete(self: Org): Promise<DeleteResult> {
    return await Org.delete(self.id);
  }

  async updatePhoto(
    self: Org,
    file: FileUpload,
    prop: "avatarUrl" | "coverUrl"
  ): Promise<string> {
    file = await file;

    const nameParts = file.filename.split(".");
    const extension = nameParts[nameParts.length - 1];
    const filename = v4() + "." + extension;

    file.filename = "/orgs/" + self.id + "/" + filename;

    const photoUrl = await this.storageService.uploadPublicFile(file);
    self[prop] = photoUrl;
    await self.save();

    return photoUrl + "?dummy=1234";
  }
}
