import {
  DeleteResult,
  FindManyOptions,
  FindOneOptions,
  getConnection,
  RemoveOptions,
  SaveOptions,
  UpdateResult,
} from "typeorm";
import { Service } from "typedi";
import OrgService from "./Org";
import EventService from "./Event";
import { User } from "../entities/User";
import { Org } from "../entities/Org";
import { UserInputError } from "apollo-server-errors";
import { Event } from "../entities/Event";

@Service()
export default class UserService {
  constructor(
    private orgService: OrgService,
    private eventService: EventService
  ) {}

  public async findAll(options?: FindManyOptions<User>): Promise<User[]> {
    return await User.find(options);
  }

  public async findByEmail(
    userEmail: string,
    options?: FindOneOptions<User>
  ): Promise<User | null> {
    return await User.findOne({ email: userEmail }, options);
  }

  public async findById(
    id: string,
    options?: FindOneOptions<User>
  ): Promise<User | null> {
    return await User.findOneOrFail(id, options);
  }

  // public async create(data: Partial<User>): Promise<User> {
  //   return User.create(data);
  // }

  public async delete(
    id: string,
    options?: RemoveOptions
  ): Promise<DeleteResult> {
    return await User.delete(id, options);
  }

  public async updateProfile(
    user: User,
    data: Partial<
      Pick<User, "name" | "phone_number" | "avatarUrl" | "interests" | "dob">
    >,
    options?: SaveOptions
  ): Promise<UpdateResult> {
    return await User.update(user.id, data, options);
  }

  public async subscribeOrg(user: User, org_id: string): Promise<void> {
    let org: Org;
    try {
      org = await this.orgService.findById(org_id);
    } catch (err) {
      throw new UserInputError("No such organization with id: " + org_id);
    }

    (await user.subscribing_orgs).push(org);

    await user.save();
  }

  public async subscribeEvent(user: User, event_id: string): Promise<void> {
    let event: Event;
    try {
      event = await this.eventService.findById(event_id);
    } catch (err) {
      throw new UserInputError("No such event with id: " + event_id);
    }

    (await user.subscribing_events).push(event);

    await user.save();
  }

  public async unsubscribeOrg(user: User, org_id: string): Promise<void> {
    user.subscribing_orgs = Promise.resolve(
      (await user.subscribing_orgs).filter((o) => o.id !== org_id)
    );

    await user.save();
  }

  public async unsubscribeEvent(user: User, event_id: string): Promise<void> {
    user.subscribing_events = Promise.resolve(
      (await user.subscribing_events).filter((e) => e.id !== event_id)
    );

    await user.save();
  }

  public async followUser(user: User, user_id: string): Promise<void> {
    if (user.id === user_id) {
      throw new UserInputError("Đừng thẩm du");
    }
    if (
      user.followings != null &&
      (await user.followings).some((u) => u.id == user_id)
    ) {
      throw new UserInputError("Đã follow rồi");
    }
    let followee: User;
    try {
      followee = await this.findById(user_id);
    } catch (err) {
      throw new UserInputError("Không tìm thấy người dùng với id: " + user_id);
    }

    await getConnection()
      .createQueryBuilder()
      .relation(User, "followings")
      .of(user)
      .add(user_id);
  }

  public async unfollowUser(user: User, user_id: string): Promise<void> {
    if (user.id === user_id) {
      throw new UserInputError("Đừng thẩm du");
    }

    await getConnection()
      .createQueryBuilder()
      .relation(User, "followings")
      .of(user)
      .remove(user_id);
  }
}
