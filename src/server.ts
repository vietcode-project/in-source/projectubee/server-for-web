import express, { Request, Response, NextFunction } from "express";
import { ApolloServer } from "apollo-server-express";
import { createConnection, useContainer } from "typeorm";
import Container from "typedi";

import { User } from "./entities/User";
import schema from "./utils/schema";
import { v4 } from "uuid";
import Account from "./entities/Account";
import { Org } from "./entities/Org";
import axios from "axios";
import { AuthResponse } from "./types/AuthResponse";

export const server = async () => {
  const PORT = process.env.PORT || 3000;

  useContainer(Container);

  const app = express();
  // /**
  //  * typeorm setup
  //  */

  await createConnection();

  // /**
  //  * Typegraphql setup
  //  */

  app.set("trust proxy", 1);

  app.get("/confirm/:id", async (req, res) => {
    const { id: confirm_id } = req.params;

    res.redirect("https://www.projectube.org/signup/confirm?id=" + confirm_id);
  });

  const apollo = new ApolloServer({
    schema: await schema,
    playground: process.env.PLAYGROUND === "true",
    introspection: process.env.PLAYGROUND === "true",

    context: async ({ req, res }: { req: Request; res: Response }) => {
      let user: Account | null;
      if (!req.headers.authorization) return { req, res, user: null };

      //send request to auth service to verify access_token and then get the data from auth service
      const account: AuthResponse | null = await axios
        .get(process.env.AUTH_URI + "/verify", {
          headers: {
            Authorization: req.headers.authorization,
          },
        })
        .then((res) => {
          return { ...res.data };
        })
        .catch(() => {
          return null;
        });

      if (account) {
        user = await Account.findOne({ email: account.email });
      }

      if (!user) user = null;
      return {
        req,
        res,
        user,
      };
    },
  });

  apollo.applyMiddleware({
    path: "/api/gql",
    app,
    cors: {
      origin: process.env.ALLOWED_ORIGINS.split(","),
      credentials: true,
    },
  });

  app.listen({ port: PORT }, () => {
    console.log(
      `🚀 Server ready at http://localhost:${PORT}${apollo.graphqlPath}`
    );
    console.log(
      `🚀 Subscriptions ready at ws://localhost:${PORT}${apollo.subscriptionsPath}`
    );
  });
};
