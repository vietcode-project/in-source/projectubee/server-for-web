import { Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { Field, ID, ObjectType } from "type-graphql";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Org } from "./Org";
import { Application } from "./Application";

@Entity()
@ObjectType()
export class Recruitment extends AbstractBaseEntity {
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  title: string;

  @Column({ type: "timestamp", nullable: true })
  @Field({ nullable: true })
  deadline: Date;

  @Column({ nullable: true })
  @Field({ nullable: true })
  cover_url: string;

  @Column("text", { nullable: true, array: true })
  @Field(() => [String], { nullable: true })
  positions: string[];

  @Column("text", { nullable: true, array: true })
  @Field(() => [String], { nullable: true })
  screen_questions: string[];

  @Column({ default: "" })
  @Field({ nullable: true })
  job_description: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  location: string;

  @Column({ default: true })
  @Field()
  isOpen: boolean;

  @ManyToOne(() => Org, (o) => o.recruitments, { onDelete: "CASCADE" })
  @Field(() => Org)
  org: Promise<Org>;

  @OneToMany(() => Application, (a) => a.recruitment, { onDelete: "CASCADE" })
  @Field(() => [Application], { nullable: true })
  applications: Promise<Application[]>;
}
