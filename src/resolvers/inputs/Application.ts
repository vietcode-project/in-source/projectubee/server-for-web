import { Field, InputType } from "type-graphql";

@InputType()
export class CreateApplicationInput {
  @Field(() => [String], { nullable: true })
  screen_answers: string[];
}

@InputType()
export class UpdateApplicationInput {
  @Field(() => [String], { nullable: true })
  screen_answers: string[];
}
