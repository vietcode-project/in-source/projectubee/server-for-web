import { GraphQLUpload } from "apollo-server";
import { FileUpload } from "graphql-upload";
import { Field, InputType } from "type-graphql";
import { StringFilter } from "./Filter";

@InputType()
export class UpdateContactInput {
  @Field({ nullable: true })
  email: string;

  @Field({ nullable: true })
  phone_number: string;

  @Field({ nullable: true })
  website: string;

  @Field({ nullable: true })
  location: string;

  @Field({ nullable: true })
  facebook_url: string;

  @Field({ nullable: true })
  instagram_url: string;
}

@InputType()
export class CreateOrgInput {
  @Field()
  name: string;

  @Field({ nullable: true })
  contact: UpdateContactInput;

  @Field({ nullable: true })
  description: string;

  @Field(() => GraphQLUpload, { nullable: true })
  avatar: FileUpload;

  @Field(() => GraphQLUpload, { nullable: true })
  cover: FileUpload;

  @Field(() => [String])
  categories: string[];
}

@InputType()
export class UpdateDescriptionInput {
  @Field()
  title: string;

  @Field()
  content: string;
}

@InputType()
export class UpdateOrgInput {
  @Field({ nullable: true })
  name: string;

  @Field(() => GraphQLUpload, { nullable: true })
  avatar: FileUpload;

  @Field(() => GraphQLUpload, { nullable: true })
  cover: FileUpload;

  @Field(() => UpdateContactInput, { nullable: true })
  contact: UpdateContactInput;

  @Field(() => String, { nullable: true })
  bio: string;

  @Field(() => String, { nullable: true })
  org_type: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: true })
  categories: string[];
}

@InputType()
export class OrgFilterInput {
  @Field({ nullable: true })
  name: StringFilter;

  @Field(() => [String], { nullable: true })
  categories: string[];
}
