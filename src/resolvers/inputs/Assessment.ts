import { Field, InputType } from "type-graphql";

@InputType()
export class CreateAssessmentInput {
  @Field(() => Number)
  rate: number;

  @Field(() => String)
  comment: string;
}
