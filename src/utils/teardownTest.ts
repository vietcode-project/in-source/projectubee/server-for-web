import { getConnection } from "typeorm";

export default async function () {
  await getConnection().close();
}
