import { Ctx, FieldResolver, Resolver, Root } from "type-graphql";
import { Application } from "../../entities/Application";
import { Recruitment } from "../../entities/Recruitment";
import { ContextType } from "../../types/Context";

@Resolver(Recruitment)
export class RecruitmentFieldResolver {
  @FieldResolver(() => [Application], { nullable: true })
  async applications(@Ctx() ctx: ContextType, @Root() rec: Recruitment) {
    if (ctx.user.id !== (await rec.org).id) return null;
    return rec.applications;
  }
}
