import { Field, InterfaceType, ObjectType } from "type-graphql";
import { Entity, ManyToOne, TableInheritance, ChildEntity } from "typeorm";

import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { User } from "./User";

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
@InterfaceType()
export class Notification extends AbstractBaseEntity {
  @Field()
  id: string;

  @ManyToOne(() => User, (u) => u.notifications, { onDelete: "CASCADE" })
  @Field(() => User)
  user: Promise<User>;
}

@ChildEntity()
@ObjectType({ implements: Notification })
export class FollowNotification extends Notification {
  @ManyToOne(() => User, { onDelete: "SET NULL" })
  follower: Promise<User>;
}
