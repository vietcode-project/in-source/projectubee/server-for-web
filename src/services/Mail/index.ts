import path from "path";
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { Service } from "typedi";

import { User } from "../../entities/User";
import { Org } from "../../entities/Org";

@Service()
export default class MailService {
  private account: { email: string; pass: string; sendAs: string };
  private transporter: Mail;

  constructor() {
    this.account = {
      email: process.env.ACCOUNT_EMAIL,
      pass: process.env.ACCOUNT_PASSWORD,
      sendAs: process.env.ACCOUNT_SENDAS,
    };

    this.transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: this.account.email, // generated ethereal user
        pass: this.account.pass, // generated ethereal password
      },
    });
  }

  async sendEmail(
    recipient: string,
    data: {
      subject: string;
      content: string;
    }
  ) {
    return this.transporter.sendMail({
      from: `"Projectube Team" <${this.account.sendAs}>`, // sender address
      to: recipient,
      subject: data.subject,
      html: data.content,
      attachments: [
        {
          filename: "[ICON] Logo.png",
          path: path.resolve(__dirname, "../../../static/[ICON] Logo.png"),
          cid: "unique@kreata.ee", //same cid value as in the html img src
        },
      ],
    });
  }

  async sendConfirmationEmail(acc: User | Org) {
    // Create link
    const link = `${acc.signup_host}/confirm/${acc.confirm_id}`;

    // Send email with the link
    const getEmail = await import("./tempaltes/confirm");
    const emailContent = getEmail.default(acc, link);

    this.sendEmail(acc.email, {
      subject: "Your Projectube Account",
      content: emailContent,
    });
  }
}
