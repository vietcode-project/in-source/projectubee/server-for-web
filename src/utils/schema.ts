import { buildSchema } from "type-graphql";
import Container from "typedi";
import path from "path";

import { ContextType } from "../types/Context";

export default buildSchema({
  validate: false,
  resolvers: [
    path.resolve(__dirname, "..") + "/resolvers/**/*.resolver.ts",
    path.resolve(__dirname, "..") + "/resolvers/**/*.resolver.js",
  ],
  authChecker: async (
    { context }: { context: ContextType },
    roles: string[]
  ) => {
    if (!context.user) return false;

    if (roles.length === 0) {
      return true;
    }

    if (roles.includes("user")) {
      if (context.user.type === "User") return true;
    }

    if (roles.includes("org")) {
      if (context.user.type === "Org") return true;
    }

    return false;
  },
  container: Container,
});
