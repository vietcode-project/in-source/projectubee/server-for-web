import {
  Entity,
  Column,
  ManyToOne,
  OneToMany,
  ManyToMany,
  ChildEntity,
} from "typeorm";
import { User } from "./User";
import { Event } from "./Event";
import { Field, ID, ObjectType } from "type-graphql";
import { ArrayMaxSize, IsEmail, IsNumberString, IsUrl } from "class-validator";
import { Recruitment } from "./Recruitment";
import { Assessment, OrgAssessment } from "./Assessment";
import Account from "./Account";
import { Application } from "./Application";

@ObjectType()
export class Contact {
  @Column({ nullable: true })
  @IsEmail()
  @Field({ nullable: true })
  email: string;

  @Column({ nullable: true })
  @IsNumberString()
  @Field({ nullable: true })
  phone_number: string;

  @Column({ nullable: true })
  @IsUrl()
  @Field({ nullable: true })
  website: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  location: string;

  @Column({ nullable: true })
  @IsUrl()
  @Field({ nullable: true })
  facebook_url: string;

  @Column({ nullable: true })
  @IsUrl()
  @Field({ nullable: true })
  instagram_url: string;
}

@ChildEntity()
@ObjectType({ implements: Account })
export class Org extends Account {
  @Column()
  @Field()
  name!: string;

  @Column("text", { array: true, default: [] })
  @Field(() => [String], { nullable: true })
  categories: string[];

  @Column({ default: "" })
  @Field()
  description: string;

  @Column({ default: "" })
  @Field({ nullable: true })
  bio: string;

  @Column({ default: "" })
  @Field({ nullable: true })
  org_type: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  avatarUrl: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  coverUrl: string;

  @Column(() => Contact)
  @Field(() => Contact)
  contact: Contact;

  @ManyToMany(() => User, (user) => user.member_of, { onDelete: "SET NULL" })
  @Field(() => [User])
  members!: Promise<User[]>;

  @ManyToMany(() => User, (user) => user.subscribing_orgs, {
    onDelete: "SET NULL",
  })
  @Field(() => [User])
  subscribers: Promise<User[]>;

  @OneToMany(() => Event, (event) => event.organizer, { onDelete: "RESTRICT" })
  @Field(() => [Event])
  events!: Promise<Event[]>;

  @OneToMany(() => Recruitment, (a) => a.org, {
    onDelete: "SET NULL",
  })
  @Field(() => [Recruitment])
  recruitments!: Promise<Recruitment[]>;

  @OneToMany(() => OrgAssessment, (a) => a.org, { onDelete: "SET NULL" })
  @Field(() => [Assessment])
  assessments: Promise<OrgAssessment[]>;

  async addMember(user: User): Promise<boolean> {
    try {
      (await this.members).push(user);
      await this.save();
    } catch (err) {
      console.error(err);
      return false;
    }
    return true;
  }
}
