# Projectube's server for web

Đây là cái server để phục vụ riêng cho web.

## :gear: Cài đặt

- Cài đặt [Docker](https://docs.docker.com/get-docker/) về máy.

- Clone cái repo này về máy.

```bash
git clone git@gitlab.com:vietcode-project/in-source/projectubee/server-for-web.git
```

- Nếu bạn là developer của server thì sẽ cần tải các dependencies

```bash
npm i
```

## :runner: Sử dụng

### Chỉ cần chạy server (không thêm sửa xóa gì ở server)

- Sử dụng Docker để chạy server

```bash
docker compose -f ./docker-compose-dev.yml up
```

hoặc

```bash
npm run start:docker:dev
```

### Chạy để phát triển server

```bash
npm run dev
```

### Build bản production của server

```bash
npm run build
```

Sau khi build xong, bạn sẽ thấy folder /dist. Để chạy được server vừa được build, sử dụng

```bash
npm start
```

## :book: Contributing

Các bạn có thể tạo issues về bug, tính năng, hoặc bất cứ ý tưởng gì của các bạn để đóng góp cho chúng tôi.
