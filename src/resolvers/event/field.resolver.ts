import { FieldResolver, Resolver, Root } from "type-graphql";
import { Service } from "typedi";
import { Event } from "../../entities/Event";
import EventService from "../../services/Event";

@Service()
@Resolver(() => Event)
export class EventFieldResolver {
  constructor(private eventService: EventService) {}

  @FieldResolver(() => [Event])
  async related_events(@Root() self: Event): Promise<Event[]> {
    return this.eventService.getRelatedEvents(self.id);
  }
}
