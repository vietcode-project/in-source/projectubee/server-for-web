import { FindOneOptions, Raw } from "typeorm";
import { Service } from "typedi";
import Account from "../entities/Account";
import { User } from "../entities/User";
import { Org } from "../entities/Org";
import { UserRegisterInput } from "../resolvers/inputs/Auth";

@Service()
export default class AccountService {
  public async findByEmail(email: string, options?: FindOneOptions<Account>) {
    return Account.findOne({ email }, options);
  }

  public async createAccount(
    type: string,
    data: UserRegisterInput
  ): Promise<Account> {
    let acc: Account;

    if (!data.signup_host) data.signup_host = "localhost";

    if (type === "user") {
      acc = User.create({
        ...data,
        interests: data.categories,
      });
      await User.save(acc);
    } else if (type === "org") {
      acc = Org.create(data);
      await Org.save(acc);
    } else {
      throw new Error("Invalid account type");
    }

    return acc;
  }
}
