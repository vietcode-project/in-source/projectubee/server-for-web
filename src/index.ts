if (process.env.NODE_ENV !== "production") {
  require("./utils/loadEnv").loadEnv();
  console.log("Loaded env files");
}

import { server } from "./server";

// Just start the server
server().catch(console.error);
