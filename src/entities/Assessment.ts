import { Field, ObjectType, ID } from "type-graphql";
import {
  ChildEntity,
  Column,
  Entity,
  ManyToOne,
  TableInheritance,
} from "typeorm";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Event } from "./Event";
import { Org } from "./Org";
import { User } from "./User";

@Entity()
@ObjectType()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export class Assessment extends AbstractBaseEntity {
  @Field(() => ID)
  id: string;

  @Column("smallint")
  @Field()
  rate: number;

  @Column()
  @Field()
  comment: string;

  @ManyToOne(() => User, { onDelete: "CASCADE" })
  @Field(() => User, { nullable: true })
  user: Promise<User>;
}

@ChildEntity()
export class OrgAssessment extends Assessment {
  @ManyToOne(() => Org, (o) => o.assessments, { onDelete: "CASCADE" })
  org: Promise<Org>;
}

@ChildEntity()
export class EventAssessment extends Assessment {
  @ManyToOne(() => Event, (e) => e.assessments, { onDelete: "CASCADE" })
  event: Promise<Event>;
}
