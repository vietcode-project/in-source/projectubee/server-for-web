import { Column, ManyToMany, OneToMany, JoinTable, ChildEntity } from "typeorm";
import { Field, ObjectType } from "type-graphql";

import { Org } from "./Org";
import { Event } from "./Event";
import { Notification } from "./Noti";
import Account from "./Account";
import { Application } from "./Application";

@ChildEntity()
@ObjectType({ implements: Account })
export class User extends Account {
  /**
   * Description
   *
   * @Note - @Field only applies for fields that is accessible for both other
   *  users and the current user.
   */
  @Column()
  @Field(() => String)
  name: string;

  @Column()
  @Field()
  phone_number: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  avatarUrl?: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  coverUrl?: string;

  @Column("text", { array: true, default: [] })
  @Field(() => [String])
  interests: string[];

  @Column({ type: "timestamp", nullable: true })
  @Field({ nullable: true })
  dob: Date;

  @ManyToMany(() => Org, (org) => org.members, { onDelete: "SET NULL" })
  @JoinTable()
  @Field(() => [Org])
  member_of: Promise<Org[]>;

  @ManyToMany(() => Org, (org) => org.subscribers, { onDelete: "SET NULL" })
  @JoinTable()
  @Field(() => [Org])
  subscribing_orgs: Promise<Org[]>;

  @ManyToMany(() => Event, (event) => event.subscribers, {
    onDelete: "SET NULL",
  })
  @JoinTable()
  @Field(() => [Event])
  subscribing_events: Promise<Event[]>;

  @ManyToMany(() => Event, (event) => event.attendees, { onDelete: "SET NULL" })
  @JoinTable()
  @Field(() => [Event])
  attend_events: Promise<Event[]>;

  @ManyToMany(() => User, (u) => u.followings, { onDelete: "SET NULL" })
  @JoinTable()
  @Field(() => [User])
  followers: Promise<User[]>;

  @ManyToMany(() => User, (u) => u.followers, { onDelete: "SET NULL" })
  @Field(() => [User])
  followings: Promise<User[]>;

  @OneToMany(() => Application, (a) => a.user)
  @Field(() => [Application])
  applications: Promise<Application[]>;

  @OneToMany(() => Notification, (n) => n.user)
  @Field(() => [Notification])
  notifications: Promise<Notification[]>;

  @Field(() => [Org], { nullable: true })
  related_org_by_categories?: Org[];

  @Field(() => [Event], { nullable: true })
  related_event_by_categories?: Event[];

  async joinOrg(org: Org): Promise<boolean> {
    try {
      (await this.member_of).push(org);
      await this.save();
    } catch (err) {
      console.error(err);
      return false;
    }

    return true;
  }
}
