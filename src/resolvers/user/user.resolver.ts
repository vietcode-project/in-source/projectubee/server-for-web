import { Query, Resolver } from "type-graphql";
import { Service } from "typedi";

import UserService from "../../services/User";
import { User } from "../../entities/User";

@Resolver()
@Service()
export class UsersResolver {
  constructor(private userService: UserService) {}

  @Query(() => [User])
  async users() {
    return this.userService.findAll();
  }
}
