import { IsString, MaxLength } from "class-validator";
import { InputType, Field } from "type-graphql";

@InputType()
export class CreatePostInput {
  @Field()
  @MaxLength(128)
  title: string;

  @Field()
  @IsString()
  content: string;
}

@InputType()
export class UpdatePostInput {
  @Field({ nullable: true })
  title: string;

  @Field({ nullable: true })
  content: string;
}
