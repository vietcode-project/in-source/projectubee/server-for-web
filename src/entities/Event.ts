import { Entity, Column, ManyToOne, ManyToMany, OneToMany } from "typeorm";
import { Field, ID, ObjectType } from "type-graphql";

import { User } from "./User";
import { Org } from "./Org";
import { AbstractBaseEntity } from "./AbstractBaseEntity";
import { Service } from "typedi";
import { Assessment, EventAssessment } from "./Assessment";

@Entity()
@ObjectType()
@Service()
export class Event extends AbstractBaseEntity {
  @Field(() => ID)
  id: string;

  @Column()
  @Field()
  name: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  coverUrl?: string;

  @Column({ type: "timestamp", nullable: true })
  @Field({ nullable: true })
  start_time: Date;

  @Column({ nullable: true })
  @Field({ nullable: true })
  location: string;

  @Column("text", { nullable: true, array: true })
  @Field(() => [String], { nullable: true })
  categories: string[];

  @Column({ default: "" })
  @Field()
  description: string;

  @Column("text", { nullable: true })
  @Field(() => String, { nullable: true })
  register_link: string;

  @ManyToMany(() => User, (user) => user.subscribing_events, {
    onDelete: "SET NULL",
  })
  @Field(() => [User])
  subscribers: Promise<User[]>;

  @ManyToMany(() => User, (user) => user.attend_events, {
    onDelete: "SET NULL",
  })
  @Field(() => [User])
  attendees: Promise<User[]>;

  @ManyToOne(() => Org, (org) => org.events, { onDelete: "CASCADE" })
  @Field(() => Org)
  organizer: Promise<Org>;

  @Field(() => [Event])
  related_events?: Event[];

  @OneToMany(() => EventAssessment, (a) => a.event)
  @Field(() => [Assessment])
  assessments: Promise<EventAssessment[]>;
}
