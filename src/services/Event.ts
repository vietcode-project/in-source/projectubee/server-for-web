import { FileUpload } from "graphql-upload";
import { Service } from "typedi";
import {
  DeleteResult,
  FindManyOptions,
  FindOneOptions,
  getConnection,
  RemoveOptions,
  SaveOptions,
  UpdateResult,
} from "typeorm";
import { v4 } from "uuid";

import { Event } from "../entities/Event";
import { Org } from "../entities/Org";
import { CreateEventInput, UpdateEventInput } from "../resolvers/inputs/Event";
import StorageService from "./Storage";

@Service()
export default class EventService {
  constructor(private storageService: StorageService) {}

  public async create(self: Org, data: CreateEventInput): Promise<Event> {
    const newEvent = Event.create(data);
    try {
      await Event.save(newEvent);
    } catch (err) {
      throw err;
    }

    if (data.cover) {
      const url = await this.updatePhoto(newEvent, data.cover);
      self.coverUrl = url;
    }

    try {
      await getConnection()
        .createQueryBuilder()
        .relation(Event, "organizer")
        .of(newEvent)
        .set(self);

      return newEvent;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  public async findAll(options?: FindManyOptions<Event>): Promise<Event[]> {
    return await Event.find(options);
  }

  public async findById(
    id: string,
    options?: FindOneOptions<Event>
  ): Promise<Event | null> {
    return await Event.findOneOrFail(id, options);
  }

  public async update(
    self: Event | string,
    data: UpdateEventInput,
    options?: SaveOptions
  ): Promise<UpdateResult> {
    let event: Event;
    if (typeof self === "string") {
      event = await this.findById(self as string);
    } else event = self;

    if (data.cover) {
      const url = await this.updatePhoto(event, data.cover);
      event.coverUrl = url;
      await event.save();
    }

    try {
      const result = await Event.update(event.id, data, options);
      return result;
    } catch (err) {
      throw new Error("Oops");
    }
  }

  public async delete(
    id: string,
    options?: RemoveOptions
  ): Promise<DeleteResult> {
    return await Event.delete(id, options);
  }

  async updatePhoto(
    self: Event,
    file: FileUpload,
    prop: "coverUrl" = "coverUrl"
  ): Promise<string> {
    file = await file;

    const nameParts = file.filename.split(".");
    const extension = nameParts[nameParts.length - 1];
    const filename = v4() + "." + extension;

    file.filename = "/events/" + self.id + "/" + filename;

    const photoUrl = await this.storageService.uploadPublicFile(file);
    self[prop] = photoUrl;
    await self.save();

    return photoUrl;
  }

  async getRelatedEvents(event_id: string): Promise<Event[]> {
    const event = await getConnection()
      .createQueryBuilder(Event, "events")
      .where("events.id != :id", { id: event_id })
      .orderBy("RANDOM()")
      .limit(4)
      .getMany();
    console.log(event);
    return event;
    // return axios
    //   .get(process.env.REC_SERVER_URI + "/events/" + event_id)
    //   .then((res) => {
    //     return this.findAll({
    //       where: {
    //         id: In(res.data.list_of_recommend),
    //       },
    //     });
    //   })
    //   .catch((err) => {
    //     console.error(err);
    //     return [];
    //   });
  }
}
