import { AuthenticationError, UserInputError } from "apollo-server";
import { Arg, Authorized, Ctx, Mutation, Resolver } from "type-graphql";
import { Service } from "typedi";

import { Application } from "../../entities/Application";
import { Recruitment } from "../../entities/Recruitment";
import { User } from "../../entities/User";
import { ApplicationService } from "../../services/Application";
import RecruitService from "../../services/Recruitment";
import { ContextType } from "../../types/Context";
import {
  CreateApplicationInput,
  UpdateApplicationInput,
} from "../inputs/Application";

@Service()
@Resolver()
export class ApplicationCRUDResolver {
  constructor(
    private applicationService: ApplicationService,
    private recruitService: RecruitService
  ) {}

  @Authorized("user")
  @Mutation(() => Application)
  async create_application(
    @Ctx() ctx: ContextType,
    @Arg("data") data: CreateApplicationInput,
    @Arg("recruitment_id") recruitment_id: string
  ) {
    const recruitment: Recruitment = await this.recruitService.findById(
      recruitment_id
    );
    if (!recruitment)
      throw new UserInputError(
        "Không tìm thấy tin tuyển dụng với id: " + recruitment_id
      );
    return this.applicationService.create(ctx.user as User, recruitment, data);
  }

  @Authorized()
  @Mutation(() => Boolean)
  async delete_application(
    @Ctx() ctx: ContextType,
    @Arg("application_id") application_id: string
  ) {
    const app = await this.applicationService.findById(application_id);
    if (!app)
      throw new UserInputError(
        "Không tìm thấy hồ sơ với id: " + application_id
      );
    if ((await app.user).id !== ctx.user.id) throw new Error("Forbidden");

    try {
      await this.applicationService.delete(application_id);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }
}
