import { AuthenticationError } from "apollo-server";
import { Service } from "typedi";
import Account from "../entities/Account";
import AccountService from "./Account";
import axios from "axios";
import { Tokens } from "../resolvers/Auth.resolver";
import { UserRegisterInput } from "../resolvers/inputs/Auth";

interface NewAccData extends UserRegisterInput {
  type: string;
  signup_host: string;
}

@Service()
export class AuthService {
  /**
   * Description
   *
   */

  constructor(private accountService: AccountService) {}

  public async register(newAccData: NewAccData): Promise<Account> {
    return axios
      .post(process.env.AUTH_URI + "/user", {
        email: newAccData.email,
        password: newAccData.password,
        role: newAccData.type,
      })
      .then(async (res) => {
        if (!res.data.confirm_id) {
          throw new Error("Something wrong when creating confirm id");
        }
        //create new account on server-for-web database after successful registering auth service
        const newAcc = await this.accountService.createAccount(
          newAccData.type,
          {
            ...newAccData,
          }
        );

        if (!newAcc) {
          throw new Error("Somthing wrong when creating new account");
        }
        newAcc.confirm_id = res.data.confirm_id;
        return newAcc;
      })
      .catch((err) => {
        if (err.response?.data?.message === "Email has already been used") {
          throw new Error("Account already exists");
        } else {
          throw new Error(`${err.response?.data?.message}`);
        }
      });
  }

  public async signinWithCredential(
    email: string,
    password: string
  ): Promise<Tokens> {
    let token: Tokens;

    return axios
      .post(process.env.AUTH_URI + "/token", {
        email: email,
        password: password,
      })
      .then((res) => {
        token = { ...res.data };
        return token;
      })
      .catch((err) => {
        console.log(err);
        if (err.response?.data?.message === "Account hasn't been confirmed yet")
          throw new AuthenticationError("Account hasn't been confirmed yet");
        else {
          throw new AuthenticationError("Invalid email or password");
        }
      });
  }

  public async signinWithConfirmID(confirm_id: string): Promise<Tokens> {
    return axios
      .post(process.env.AUTH_URI + "/confirm", {
        confirm_id: confirm_id,
      })
      .then(async (res) => {
        if (!res.data.email) {
          throw new Error("Something wrong with email for confirm");
        }
        let user: Account = await this.accountService.findByEmail(
          res.data.email
        );
        if (!user) throw new Error("Can't find email");

        user.confirmed = true;

        await user.save();
        return {
          access_token: res.data.access_token,
          refresh_token: res.data.refresh_token,
        };
      })
      .catch((err) => {
        console.log(err);
        throw new Error("Something wrong");
      });
  }

  public async signinWithToken(refresh_token: string): Promise<Tokens> {
    return axios
      .post(process.env.AUTH_URI + "/refresh", undefined, {
        headers: { Authorization: "Bearer " + refresh_token },
      })
      .then((res) => {
        return {
          access_token: res.data.access_token,
          refresh_token: refresh_token,
        };
      })
      .catch((err) => {
        console.log(err);
        throw new Error("Can't get new access token");
      });
  }

  public async signout(
    refresh_token: string,
    authorization: string
  ): Promise<boolean> {
    return axios
      .delete(process.env.AUTH_URI + "/refresh/" + refresh_token, {
        headers: {
          Authorization: authorization,
        },
      })
      .then((res) => {
        return true;
      })
      .catch((err) => {
        console.log(err);
        throw new Error("Can't sign out please try again'");
      });
  }

  public async signoutAllDevices(authorization: string): Promise<boolean> {
    return axios
      .delete(process.env.AUTH_URI + "/refresh", {
        headers: {
          Authorization: authorization,
        },
      })
      .then((res) => {
        return true;
      })
      .catch((err) => {
        console.log(err);
        throw new Error("Can't sign out please try again'");
      });
  }

  public async getNewConfirmID(email: string): Promise<string> {
    return axios
      .post(process.env.AUTH_URI + "/resend", {
        email: email,
      })
      .then((res) => {
        return res.data.confirm_id;
      })
      .catch((err) => {
        console.log(err);
        if (
          err.response?.data?.message === "Account has already been confirmed"
        ) {
          throw new Error("Account has already been confirmed");
        } else {
          throw new Error("Account not found");
        }
      });
  }
}
