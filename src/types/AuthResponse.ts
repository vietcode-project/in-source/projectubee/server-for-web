import { Field, ObjectType} from "type-graphql";

@ObjectType()
export class AuthResponse {
  @Field()
  id: number;

  @Field()
  email: string;

  @Field()
  role: string;
}
