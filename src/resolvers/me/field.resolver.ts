import { FieldResolver, Resolver, Root } from "type-graphql";
import { Service } from "typedi";
import { FindOneOptions, Raw } from "typeorm";
import { Org } from "../../entities/Org";
import { User } from "../../entities/User";
import { Event } from "../../entities/Event";
import EventService from "../../services/Event";
import OrgService from "../../services/Org";

@Service()
@Resolver(() => User)
export class MeFieldResolver {
  constructor(
    private orgService: OrgService,
    private eventService: EventService
  ) {}

  @FieldResolver(() => [Org])
  async related_org_by_categories(@Root() self: User): Promise<Org[]> {
    if (!self.interests.length) {
      return [];
    } else {
      let randomInterests =
        self.interests[Math.floor(Math.random() * self.interests.length)];

      let _filter: FindOneOptions<Org>["where"] = {};
      if (randomInterests) {
        const cateFilter = Raw(
          (alias) => `${alias} && '{"${randomInterests}"}'`
        );

        _filter.categories = cateFilter;
      }
      return await this.orgService.findAll({ where: _filter });
    }
  }

  @FieldResolver(() => [Event])
  async related_event_by_categories(@Root() self: User): Promise<Event[]> {
    if (!self.interests.length) {
      return [];
    } else {
      let randomInterests =
        self.interests[Math.floor(Math.random() * self.interests.length)];

      let _filter: FindOneOptions<Org>["where"] = {};
      if (randomInterests) {
        const cateFilter = Raw(
          (alias) => `${alias} && '{"${randomInterests}"}'`
        );

        _filter.categories = cateFilter;
      }
      return await this.eventService.findAll({ where: _filter });
    }
  }
}
