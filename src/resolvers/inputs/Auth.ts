import { IsEmail, Length } from "class-validator";
import { InputType, Field } from "type-graphql";

@InputType()
export class UserRegisterInput {
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @Length(6, 16)
  password: string;

  @Field()
  name: string;

  @Field()
  phone_number: string;

  @Field(() => [String], { nullable: true })
  categories: string[];

  signup_host: string;
}
