import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Equal, FindOneOptions, ILike, Not, Raw } from "typeorm";
import { Service } from "typedi";

import { Org } from "../../entities/Org";
import OrgService from "../../services/Org";
import { ContextType } from "../../types/Context";
import { OrgFilterInput } from "../inputs/Org";

@Service()
@Resolver()
export class OrgCRUDResolver {
  constructor(private orgService: OrgService) {}

  @Query(() => [Org])
  async orgs(
    @Arg("limit", { nullable: true }) take: number = 10,
    @Arg("offset", { nullable: true }) skip: number = 0,
    @Arg("filter", { nullable: true }) filter: OrgFilterInput
  ) {
    let _filter: FindOneOptions<Org>["where"] = {};

    if (filter && filter.name) {
      let nameFilter;
      if (filter.name.eq) {
        nameFilter = Equal(filter.name.eq);
      } else if (filter.name.re) {
        nameFilter = ILike(`%${filter.name.re}%`);
      }
      _filter.name = nameFilter;
    }

    if (filter && filter.categories?.length) {
      const cateFilter = Raw(
        (alias) =>
          `${alias} @> '{${filter.categories.reduce(
            (acc, cur) => acc + `,"${cur}"`
          )}}'`
      );

      _filter.categories = cateFilter;
    }

    return await this.orgService.findAll({
      take,
      skip,
      where: { ..._filter, confirmed: Not(false) },
    });
  }

  @Query(() => Org)
  async org(@Arg("id") id: string) {
    return this.orgService.findById(id);
  }

  @Authorized("org")
  @Mutation(() => Boolean)
  async delete_org(@Ctx() ctx: ContextType) {
    if (!ctx.user) throw new Error("Đăng nhập ik");

    try {
      await this.orgService.delete(ctx.user as Org);
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}
