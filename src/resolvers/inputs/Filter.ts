import { Field, InputType } from "type-graphql";

@InputType()
export class StringFilter {
  @Field({ nullable: true })
  eq: string;

  @Field({ nullable: true })
  re: string;
}
