import { Service } from "typedi";
import {
  DeleteResult,
  FindManyOptions,
  FindOneOptions,
  getConnection,
} from "typeorm";
import {
  Assessment,
  EventAssessment,
  OrgAssessment,
} from "../entities/Assessment";
import { User } from "../entities/User";
import { Event } from "../entities/Event";
import { CreateAssessmentInput } from "../resolvers/inputs/Assessment";
import { Org } from "../entities/Org";

@Service()
export default class AssessmentService {
  async findAll(options: FindManyOptions<Assessment>) {
    return Assessment.find(options);
  }

  async findById(
    id: string,
    options?: FindOneOptions<Assessment>
  ): Promise<Assessment | null> {
    return Assessment.findOne(id, options);
  }

  async createOrgAssessment(
    user: User,
    org: Org,
    data: CreateAssessmentInput
  ): Promise<OrgAssessment> {
    const newAss = OrgAssessment.create({
      ...data,
    });
    await Assessment.save(newAss);

    await Promise.all([
      getConnection()
        .createQueryBuilder()
        .relation(OrgAssessment, "org")
        .of(newAss)
        .set(org),
      getConnection()
        .createQueryBuilder()
        .relation(OrgAssessment, "user")
        .of(newAss)
        .set(user),
    ]);

    return newAss;
  }

  async createEventAssessment(
    user: User,
    event: Event,
    data: CreateAssessmentInput
  ): Promise<EventAssessment> {
    const newAss = EventAssessment.create({
      ...data,
    });
    await Assessment.save(newAss);

    await Promise.all([
      getConnection()
        .createQueryBuilder()
        .relation(EventAssessment, "event")
        .of(newAss)
        .set(event),
      getConnection()
        .createQueryBuilder()
        .relation(EventAssessment, "user")
        .of(newAss)
        .set(user),
    ]);

    return newAss;
  }

  async deleteAssessment(assessment_id: string): Promise<DeleteResult> {
    return await Assessment.delete(assessment_id);
  }

  async updateAssessment(assessment_id: string, data: CreateAssessmentInput) {
    return await Assessment.update(assessment_id, data);
  }
}
