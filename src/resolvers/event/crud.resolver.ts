import { AuthenticationError, UserInputError } from "apollo-server";
import { Equal, FindOneOptions, ILike, Raw } from "typeorm";
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Service } from "typedi";

import { Event } from "../../entities/Event";
import {
  CreateEventInput,
  EventFilterInput,
  UpdateEventInput,
} from "../inputs/Event";
import { ContextType } from "../../types/Context";
import EventService from "../../services/Event";
import { Org } from "../../entities/Org";

@Service()
@Resolver()
export class EventCRUDResolver {
  constructor(private eventService: EventService) {}

  @Authorized("org")
  @Mutation(() => Event)
  async create_event(
    @Ctx() ctx: ContextType,
    @Arg("data") data: CreateEventInput
  ): Promise<Event> {
    return this.eventService.create(ctx.user as Org, data);
  }

  @Query(() => [Event])
  async events(
    @Arg("offset", { nullable: true }) skip: number = 0,
    @Arg("limit", { nullable: true }) take: number = 10,
    @Arg("filter", { nullable: true }) filter: EventFilterInput
  ): Promise<Event[]> {
    let _filter: FindOneOptions<Event>["where"] = {};

    if (filter && filter.name) {
      let nameFilter;
      if (filter.name.eq) {
        nameFilter = Equal(filter.name.eq);
      } else if (filter.name.re) {
        nameFilter = ILike(`%${filter.name.re}%`);
      }
      _filter.name = nameFilter;
    }

    if (filter && filter.location) {
      let locationFilter;
      if (filter.location.eq) {
        locationFilter = Equal(filter.location.eq);
      } else if (typeof filter.location.re !== "undefined") {
        locationFilter = ILike(`%${filter.location.re}%`);
      }
      _filter.location = locationFilter;
    }

    if (filter && filter.categories?.length) {
      const cateFilter = Raw(
        (alias) =>
          `${alias} && '{${filter.categories.reduce(
            (acc, cur) => acc + `,"${cur}"`
          )}}'`
      );

      _filter.categories = cateFilter;
    }

    if (filter && filter.start_time) {
      let timeFilter;

      timeFilter = Raw((alias) => `CAST (${alias} AS DATE) = :date `, {
        date: filter.start_time,
      });

      _filter.start_time = timeFilter;
    }

    return this.eventService.findAll({
      skip,
      take,
      where: _filter,
      order: { createdAt: "DESC" },
    });
  }

  @Query(() => Event)
  async event(@Arg("id") id: string): Promise<Event> {
    return this.eventService.findById(id);
  }

  @Authorized("org")
  @Mutation(() => Event)
  async update_event(
    @Ctx() ctx: ContextType,
    @Arg("event_id") event_id: string,
    @Arg("data") data: UpdateEventInput
  ) {
    const event = await this.eventService.findById(event_id);
    if (!event || (await event.organizer).id !== ctx.user.id)
      throw new AuthenticationError("Forbidden");

    await this.eventService.update(event.id, data);

    return this.eventService.findById(event_id);
  }

  @Authorized("org")
  @Mutation(() => Boolean)
  async delete_event(
    @Ctx() ctx: ContextType,
    @Arg("event_id") event_id: string
  ): Promise<boolean> {
    const event: Event = await this.eventService.findById(event_id);
    if (!event || (await event.organizer).id !== ctx.user.id)
      throw new AuthenticationError("Forbidden");

    try {
      await this.eventService.delete(event_id);
      return true;
    } catch (e) {
      return false;
    }
  }
}
