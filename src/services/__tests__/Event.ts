test.todo("Create an event a normal event without cover");
test.todo("Create an event a normal event with cover");
test.todo("Create an event with less than 3 categories");
test.todo("Create an event with more than 3 categories");
test.todo("Create an event without name");
test.todo("Create an event without location");
test.todo("Create an event without start_time");
test.todo("Create an event without description");

test.todo("Find an event that exists");
test.todo("Find an event that does not exist");
test.todo("Find all events");

test.todo("Update an event that exists with valid input");
test.todo("Update an event that exists with invalid input");
test.todo("Update an event that does not exist");

test.todo("Delete an event that exists");
test.todo("Delete an event that does not exist");
