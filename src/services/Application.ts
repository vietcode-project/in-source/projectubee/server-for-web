import { Service } from "typedi";
import {
  DeleteResult,
  FindManyOptions,
  getConnection,
  RemoveOptions,
  SaveOptions,
  UpdateResult,
} from "typeorm";
import { Application } from "../entities/Application";
import { Recruitment } from "../entities/Recruitment";
import { User } from "../entities/User";
import { CreateApplicationInput } from "../resolvers/inputs/Application";

@Service()
export class ApplicationService {
  public async findById(id: string): Promise<Application> {
    return Application.findOneOrFail(id);
  }

  public async create(
    user: User,
    recruitment: Recruitment,
    data: CreateApplicationInput
  ): Promise<Application> {
    const newApp = Application.create({ ...data });

    await Application.save(newApp);

    await Promise.all([
      getConnection()
        .createQueryBuilder()
        .relation(Application, "user")
        .of(newApp)
        .set(user),
      getConnection()
        .createQueryBuilder()
        .relation(Application, "recruitment")
        .of(newApp)
        .set(recruitment),
    ]);

    return newApp;
  }

  public async update(
    id: string,
    data: CreateApplicationInput,
    options?: SaveOptions
  ): Promise<UpdateResult> {
    return await Application.update(id, data, options);
  }

  public async delete(
    id: string,
    options?: RemoveOptions
  ): Promise<DeleteResult> {
    return await Application.delete(id, options);
  }
}
