import Container from "typedi";
import { createConnection, useContainer } from "typeorm";
import { loadEnv } from "./loadEnv";

export default async function () {
  loadEnv();
  useContainer(Container);

  const DATABASE_TYPE = "postgres";
  const DATABASE_ENTITIES = ["src/entities/**/*.ts"];

  await createConnection({
    name: "test",
    type: DATABASE_TYPE,
    database: String(process.env.DATABASE_DATABASE),
    host: String(process.env.DATABASE_HOST),
    port: Number(process.env.DATABASE_PORT),
    username: String(process.env.DATABASE_USERNAME),
    password: String(process.env.DATABASE_PASSWORD),
    entities: DATABASE_ENTITIES,
    synchronize: true,
    dropSchema: true,
    logging: false,
    ssl: process.env.DATABASE_SSL === "true" && { rejectUnauthorized: false },
  });
}
