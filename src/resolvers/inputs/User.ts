import { GraphQLUpload } from "apollo-server";
import { FileUpload } from "graphql-upload";
import { InputType, Field } from "type-graphql";

@InputType()
export class UpdateProfileInput {
  @Field(() => String, { nullable: true })
  name: string;

  @Field(() => GraphQLUpload, { nullable: true })
  avatar?: FileUpload;

  @Field(() => String, { nullable: true })
  phone_number: string;

  @Field(() => [String], { nullable: true })
  interests: string[];

  @Field(() => Date, { nullable: true })
  dob: Date;
}
