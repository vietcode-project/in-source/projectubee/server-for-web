import { AuthenticationError, UserInputError } from "apollo-server-express";
import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { Service } from "typedi";
import { Assessment, OrgAssessment } from "../../entities/Assessment";
import { Org } from "../../entities/Org";
import { Event } from "../../entities/Event";
import AssessmentService from "../../services/Assessment";
import OrgService from "../../services/Org";
import EventService from "../../services/Event";

import { ContextType } from "../../types/Context";
import { CreateAssessmentInput } from "../inputs/Assessment";
import { User } from "../../entities/User";

@Service()
@Resolver()
export class AssessmentResolver {
  constructor(
    private assessmentService: AssessmentService,
    private orgService: OrgService,
    private eventService: EventService
  ) {}

  @Authorized("user")
  @Mutation(() => Assessment)
  async create_org_assessment(
    @Ctx() ctx: ContextType,
    @Arg("org_id") org_id: string,
    @Arg("data") data: CreateAssessmentInput
  ) {
    let org: Org;

    try {
      org = await this.orgService.findById(org_id);
    } catch (err) {
      throw new UserInputError("Không tìm thấy tổ chức với id: " + org_id);
    }

    return this.assessmentService.createOrgAssessment(
      ctx.user as User,
      org,
      data
    );
  }

  @Authorized("user")
  @Mutation(() => Assessment)
  async create_event_assessment(
    @Ctx() ctx: ContextType,
    @Arg("event_id") event_id: string,
    @Arg("data") data: CreateAssessmentInput
  ) {
    let event: Event;
    try {
      event = await this.eventService.findById(event_id);
    } catch (err) {
      throw new UserInputError("Không tìm thấy sự kiện với id: " + event_id);
    }

    return this.assessmentService.createEventAssessment(
      ctx.user as User,
      event,
      data
    );
  }

  @Authorized("user")
  @Mutation(() => Boolean)
  async delete_assessment(
    @Ctx() ctx: ContextType,
    @Arg("assessment_id") assessment_id: string
  ) {
    const targetAssessment = this.assessmentService.findById(assessment_id);
    if ((await (await targetAssessment).user).id == ctx.user.id) {
      try {
        await this.assessmentService.deleteAssessment(assessment_id);
        return true;
      } catch (e) {
        return false;
      }
    } else throw new AuthenticationError("Forbidden");
  }

  @Authorized("user")
  @Mutation(() => Assessment)
  async update_assessment(
    @Ctx() ctx: ContextType,
    @Arg("assessment_id") assessment_id: string,
    @Arg("data") data: CreateAssessmentInput
  ) {
    const targetAssessment = this.assessmentService.findById(assessment_id);
    if ((await (await targetAssessment).user).id == ctx.user.id) {
      await this.assessmentService.updateAssessment(assessment_id, data);
      return await this.assessmentService.findById(assessment_id);
    } else throw new AuthenticationError("Forbidden");
  }
}
